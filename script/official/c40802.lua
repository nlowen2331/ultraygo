--リビングデッドの呼び声
local s,id=GetID()
function s.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetHintTiming(0,TIMING_STANDBY_PHASE)
	e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e1:SetCondition(s.condition)
	e1:SetTarget(s.target)
	e1:SetOperation(s.operation)
	c:RegisterEffect(e1)
	--Destroy2
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_FIELD)
	e4:SetRange(LOCATION_SZONE)
	e4:SetCode(EVENT_LEAVE_FIELD)
	e4:SetCondition(s.descon2)
	e4:SetOperation(s.desop2)
	c:RegisterEffect(e4)
	--end phase
    local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e2:SetRange(LOCATION_SZONE)
	e2:SetCode(EVENT_PHASE+PHASE_END)
	e2:SetCountLimit(1)
	e2:SetOperation(s.addop)
    c:RegisterEffect(e2)
end

function s.typeReducer(c)
	if c:IsType(TYPE_SPELL) then return TYPE_SPELL end
	if c:IsType(TYPE_MONSTER) then return TYPE_MONSTER end
	if c:IsType(TYPE_TRAP) then return TYPE_TRAP end
end

function s.thcheck(g)
	return g:GetClassCount(s.typeReducer)==3
end

function s.addop(e,tp,eg,ep,ev,re,r,rp)
    local c=e:GetHandler()
    Duel.SendtoGrave(c,REASON_EFFECT)
    local g=Duel.GetMatchingGroup(s.addfilter,tp,LOCATION_DECK,0,nil)
	if #g<1 then return end
	local tg=aux.SelectUnselectGroup(g,e,tp,1,3,s.thcheck,1,tp,HINTMSG_ATOHAND)
    if #tg>0 then
		Duel.SendtoHand(tg,nil,REASON_EFFECT)
		Duel.ConfirmCards(1-tp,tg)
	end
end

function s.addfilter(c)
	return c:IsSetCard(0xee) and c:IsAbleToHand()
end

function s.cfilter(c)
	return c:IsFaceup() and c:IsCode(40803)
end
function s.condition(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetCurrentPhase()==PHASE_STANDBY and Duel.GetTurnPlayer()~=tp and Duel.GetFlagEffect(tp, 9652932)<1
end
function s.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	local c = e:GetHandler()
	if chkc then return chkc:IsControler(tp) and chkc:IsLocation(LOCATION_MZONE) and chkc:IsFaceup() and s.cfilter(chkc) end
	if chk==0 then return Duel.IsExistingTarget(s.cfilter,tp,LOCATION_MZONE,0,2,nil) end
	Duel.Hint(HINT_SELECTMSG,tp,aux.Stringid(id,0))
	local g=Duel.SelectTarget(tp,s.cfilter,tp,LOCATION_MZONE,0,2,2,nil)
	if e:IsHasType(EFFECT_TYPE_ACTIVATE) then
		Duel.SetChainLimit(aux.FALSE)
	end
end
function s.operation(e,tp,eg,ep,ev,re,r,rp)
	Duel.RegisterFlagEffect(tp,9652932,RESET_PHASE+PHASE_END,0,1) 
	local c=e:GetHandler()
	local g=Duel.GetTargetCards(e)
	local sg = Group.CreateGroup()
	for tc in aux.Next(g) do
		if tc:IsRelateToEffect(e) and tc:IsFaceup() and c:IsRelateToEffect(e) then
			c:SetCardTarget(tc)
			sg:AddCard(tc)
		end
	end
	if #sg<2 then Duel.SendtoGrave(e:GetHandler(), REASON_RULE) end
end

function s.descon2(e,tp,eg,ep,ev,re,r,rp)
	local tcg=e:GetHandler():GetCardTarget()
    local flag = false
    for tc in aux.Next(tcg) do
        if tc and eg:IsContains(tc) then
           flag=true
       end
   end
	return flag
end
function s.desop2(e,tp,eg,ep,ev,re,r,rp)
	Duel.SendtoGrave(e:GetHandler(), REASON_EFFECT)
end
