--幽鬼うさぎ
local s,id=GetID()
function s.initial_effect(c)
    c:EnableReviveLimit()
	Fusion.AddProcMixRep(c,true,true,s.mfilter2,1,99,s.mfilter1)
	--draw
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_TRIGGER_F+EFFECT_TYPE_SINGLE)
	e1:SetCode(EVENT_SPSUMMON_SUCCESS)
	e1:SetCondition(s.spcon)
	e1:SetTarget(s.sptg)
	e1:SetOperation(s.spop)
	c:RegisterEffect(e1)
	--destroy
	--destroy after battled
	local e4=Effect.CreateEffect(c)
	e4:SetCategory(CATEGORY_DESTROY)
	e4:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e4:SetCode(EVENT_BATTLED)
	e4:SetCondition(s.descon)
	e4:SetOperation(s.desop)
    c:RegisterEffect(e4)
end
function s.mfilter2(c,fc,sumtype,tp)
	return c:IsSetCard(0xa9,fc,sumtype,tp)
end
function s.mfilter1(c,fc,sumtype,tp)
	return c:IsSetCard(0xc3,fc,sumtype,tp) 
end
function s.spcon(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():IsSummonType(SUMMON_TYPE_FUSION)
end
function s.countfilter(c)
	return c:IsSetCard(0xa9,fc,sumtype,tp) 
end
function s.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
	local c = e:GetHandler()
	local matGroup = c:GetMaterial()
    local d = 0
    if matGroup~=nil then d = matGroup:FilterCount(s.countfilter,nil) end
	if chk==0 then return true end
	Duel.SetTargetPlayer(tp)
	Duel.SetTargetParam(d)
	Duel.SetOperationInfo(0,CATEGORY_DRAW,nil,0,tp,d)
	if d<2 then return end
	Duel.SetOperationInfo(0,CATEGORY_TODECK,nil,1,tp,d-1)
end
function s.spop(e,tp,eg,ep,ev,re,r,rp)
	local p,d=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER,CHAININFO_TARGET_PARAM)
	Duel.Draw(p,d,REASON_EFFECT)
	if d<2 then return end
	Duel.BreakEffect()
	Duel.Hint(HINT_SELECTMSG,p,HINTMSG_TODECK)
	local g=Duel.SelectMatchingCard(p,Card.IsAbleToDeck,p,LOCATION_HAND,0,d-1,d-1,nil)
	if #g==0 then return end
	local dg =Duel.SendtoDeck(g,nil,2,REASON_EFFECT)
	Duel.ShuffleDeck(p)
end
function s.descon(e,tp,eg,ep,ev,re,r,rp)
	local att= Duel.GetAttacker()
    local bt = att:GetBattleTarget()
	return att and bt
end

function s.desop(e,tp,eg,ep,ev,re,r,rp)
	local att = Duel.GetAttacker()
    local bt = att:GetBattleTarget()
    local c = e:GetHandler()
    local a,b
    if att==c then
        a=att
        b=bt
    else
        b=att
        a=bt
    end
	if not b or not b:IsRelateToBattle() then return false end
    Duel.Destroy(b,REASON_EFFECT)
end