local s,id=GetID()
function s.initial_effect(c)
	--xyz summon
	Xyz.AddProcedure(c,nil,2,2,nil,nil,99)
    c:EnableReviveLimit()
	--atk,def
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetRange(LOCATION_MZONE)
	e1:SetTargetRange(0,LOCATION_MZONE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetValue(s.val)
	c:RegisterEffect(e1)
	local e2=e1:Clone()
	e2:SetCode(EFFECT_UPDATE_DEFENSE)
	c:RegisterEffect(e2)
    --maintenance
	local e6=Effect.CreateEffect(c)
	e6:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e6:SetCode(EVENT_PHASE+PHASE_END)
	e6:SetRange(LOCATION_MZONE)
	e6:SetCountLimit(1)
	e6:SetCondition(s.matcon)
	e6:SetOperation(s.matop)
	c:RegisterEffect(e6)
end

function s.val(e,c)
	return e:GetHandler():GetOverlayCount()*-300
end

function s.matcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetTurnPlayer()~=tp
end

function s.matop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
    if c:CheckRemoveOverlayCard(tp, 1, REASON_EFFECT) and Duel.SelectYesNo(tp, aux.Stringid(id, 0)) then
        c:RemoveOverlayCard(tp, 1, 1, REASON_EFFECT)
    else
        Duel.SendtoGrave(c, REASON_EFFECT)
    end
end
