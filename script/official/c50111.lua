local s, id = GetID()
function s.initial_effect(c)
	--Activate
	local e1 = Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_ATKCHANGE)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetTarget(s.target)
	e1:SetOperation(s.activate)
	c:RegisterEffect(e1)
end
s.listed_names={CARD_DARK_MAGICIAN_GIRL,CARD_DARK_MAGICIAN}
function s.filter1(c, tp)
	local code = c:GetCode()
	return (c:IsCode(CARD_DARK_MAGICIAN_GIRL) or c:IsCode(CARD_DARK_MAGICIAN)) and c:IsFaceup() and c:GetAttack() > 0 and
		Duel.IsExistingMatchingCard(s.filter2, tp, LOCATION_MZONE, 0, 1, nil, code)
end

function s.filter2(c, code)
	return (c:IsCode(CARD_DARK_MAGICIAN_GIRL) or c:IsCode(CARD_DARK_MAGICIAN)) and c:IsFaceup() and not c:IsCode(code)
end

function s.target(e, tp, eg, ep, ev, re, r, rp, chk, chkc)
	if chkc then
		return false
	end
	local ft = Duel.GetLocationCount(1 - tp, LOCATION_MZONE)
	if chk == 0 then
		return Duel.IsExistingTarget(s.filter1, tp, LOCATION_MZONE, 0, 1, nil, tp)
	end
	Duel.Hint(HINT_SELECTMSG, tp, aux.Stringid(id, 0))
	local g1 = Duel.SelectTarget(tp, s.filter1, tp, LOCATION_MZONE, 0, 1, 1, nil, tp)
	local tc = g1:GetFirst()
	local code = tc:GetCode()
	e:SetLabelObject(tc)
	Duel.Hint(HINT_SELECTMSG, tp, aux.Stringid(id, 1))
	local g2 = Duel.SelectTarget(tp, s.filter2, tp, LOCATION_MZONE, 0, 1, 1, tc, code)
end
function s.activate(e, tp, eg, ep, ev, re, r, rp)
	local hc = e:GetLabelObject()
	local g = Duel.GetChainInfo(0, CHAININFO_TARGET_CARDS)
	local tc = g:GetFirst()
	if tc == hc then
		tc = g:GetNext()
	end
	if hc:IsFaceup() and hc:IsRelateToEffect(e) and tc:IsFaceup() and tc:IsRelateToEffect(e) then
		local atk = hc:GetBaseAttack()
		local e1 = Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_SET_ATTACK_FINAL)
		e1:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
		e1:SetValue(0)
		hc:RegisterEffect(e1)
		local e3 = Effect.CreateEffect(e:GetHandler())
		e3:SetType(EFFECT_TYPE_SINGLE)
		e3:SetCode(EFFECT_AVOID_BATTLE_DAMAGE)
		e3:SetProperty(EFFECT_CANNOT_DISABLE)
		e3:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
		e3:SetValue(1)
		hc:RegisterEffect(e3)
		if not hc:IsImmuneToEffect(e1) then
			local e2 = Effect.CreateEffect(e:GetHandler())
			e2:SetType(EFFECT_TYPE_SINGLE)
			e2:SetCode(EFFECT_UPDATE_ATTACK)
			e2:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
			e2:SetValue(atk)
			tc:RegisterEffect(e2)
			local e4 = Effect.CreateEffect(e:GetHandler())
			e4:SetType(EFFECT_TYPE_SINGLE)
			e4:SetCode(EFFECT_INDESTRUCTABLE_COUNT)
			e4:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
			e4:SetCountLimit(1)
			e4:SetValue(s.valcon)
			e4:SetReset(RESET_EVENT + RESETS_STANDARD + RESET_PHASE + PHASE_END)
			tc:RegisterEffect(e4)
		end
	end
end
function s.valcon(e,re,r,rp)
	return (r&REASON_BATTLE+REASON_EFFECT)~=0
end
