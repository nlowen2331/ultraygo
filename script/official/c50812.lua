--宝玉獣 サファイア・ペガサス
local s,id=GetID()
function s.initial_effect(c)
	--negate
	local e4=Effect.CreateEffect(c)
	e4:SetDescription(aux.Stringid(id,0))
	e4:SetCategory(CATEGORY_CONTROL)
	e4:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e4:SetCode(EVENT_ATTACK_ANNOUNCE)
	e4:SetProperty(EFFECT_FLAG_CARD_TARGET)
	e4:SetRange(LOCATION_MZONE)
	e4:SetCountLimit(1)
	e4:SetCondition(s.concon)
    --e4:SetCost(s.concost)
    e4:SetTarget(s.contg)
	e4:SetOperation(s.conop)
	c:RegisterEffect(e4)
end

function s.concon(e,tp,eg,ep,ev,re,r,rp)
	local at=Duel.GetAttackTarget()
	return Duel.GetTurnPlayer()~=tp and at and at:IsFaceup() and at:IsSetCard(0x2A)
end

function s.cfilter(c)
	return c:IsSetCard(0x2a) and c:IsAbleToGraveAsCost()
		and (c:IsLocation(LOCATION_MZONE) or (c:IsLocation(LOCATION_HAND) and Duel.GetLocationCount(tp, LOCATION_MZONE)>0))
end

function s.concost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(s.cfilter,e:GetHandlerPlayer(),LOCATION_MZONE+LOCATION_HAND,0,1,e:GetHandler()) end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOGRAVE)
	local g=Duel.SelectMatchingCard(tp,s.cfilter,tp,LOCATION_MZONE+LOCATION_HAND,0,1,1,e:GetHandler())
	Duel.SendtoGrave(g,REASON_COST)
end
function s.contg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	local rc = Duel.GetAttacker()
	local c = e:GetHandler()
	if chkc then return chkc:IsLocation(LOCATION_MZONE) and chkc:IsControler(1-tp) 
		and chkc:GetControler()~=tp and chkc:IsControlerCanBeChanged() 
		and Duel.GetLocationCount(tp, LOCATION_MZONE)>0 end
    if chk==0 then return rc:IsLocation(LOCATION_MZONE) 
		and rc:GetControler()~=tp and rc:IsControlerCanBeChanged() end
	Duel.SetTargetCard(rc)
	Duel.SetOperationInfo(0,CATEGORY_CONTROL,rc,1,0,0)
end
function s.conop(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetLocationCount(tp, LOCATION_MZONE)<1 then return end
    local c = e:GetHandler()
	local rc = Duel.GetFirstTarget()
	if rc and rc:IsRelateToEffect(e) and rc:IsLocation(LOCATION_MZONE) 
		and rc:GetControler()~=tp then
        Duel.GetControl(rc,tp)
        local e3=Effect.CreateEffect(c)
		e3:SetType(EFFECT_TYPE_SINGLE)
		e3:SetCode(EFFECT_CANNOT_ATTACK)
		e3:SetReset(RESET_EVENT+RESETS_STANDARD-RESET_TURN_SET)
		rc:RegisterEffect(e3)
		local e1=Effect.CreateEffect(e:GetHandler())
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_CHANGE_ATTRIBUTE)
		e1:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
		e1:SetValue(ATTRIBUTE_EARTH)
		e1:SetReset(RESET_EVENT+RESETS_STANDARD-RESET_TURN_SET)
		rc:RegisterEffect(e1)
		local e4=Effect.CreateEffect(e:GetHandler())
		e4:SetType(EFFECT_TYPE_SINGLE)
		e4:SetCode(EFFECT_CANNOT_TRIGGER)
		e4:SetReset(RESET_EVENT+RESETS_STANDARD-RESET_TURN_SET)
		rc:RegisterEffect(e4)
	end
end