--重機王ドボク・ザーク
local s,id=GetID()
function s.initial_effect(c)
	--xyz summon
	Xyz.AddProcedure(c,nil,5,2)
	c:EnableReviveLimit()
	--atk
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetRange(LOCATION_MZONE)
	e1:SetTargetRange(LOCATION_MZONE,0)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetTarget(s.atktg)
	e1:SetValue(500)
	c:RegisterEffect(e1)
	local e3=e1:Clone()
	e3:SetCode(EFFECT_UPDATE_DEFENSE)
	c:RegisterEffect(e3)
    --change battle target
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e2:SetCode(EVENT_ATTACK_ANNOUNCE)
	e2:SetRange(LOCATION_MZONE)
	e2:SetCondition(s.cbcon)
    e2:SetCost(s.cbcost)
	e2:SetOperation(s.cbop)
	c:RegisterEffect(e2,false,REGISTER_FLAG_DETACH_XMAT)
end

function s.atktg(e,c)
    return e:GetHandler()~=c
end

function s.cbcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_COST) end
	e:GetHandler():RemoveOverlayCard(tp,1,1,REASON_COST)
end

function s.cbcon(e,tp,eg,ep,ev,re,r,rp)
    local attacker = Duel.GetAttacker()
    local bt = attacker:GetBattleTarget()
	return Duel.GetTurnPlayer()~=tp and e:GetHandler()~=bt and bt:IsFaceup()
end
function s.cbop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
    if not c:IsFaceup() then return end
	Duel.ChangeAttackTarget(c)
end