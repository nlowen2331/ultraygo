local s,id=GetID()
function s.initial_effect(c)
    Ritual.AddProcGreater({handler=c,lv=8,filter=s.ritualfilter,matfilter=s.matfilter})
end

function s.ritualfilter(c)
    return c:IsCode(50222) and c:IsRitualMonster()
end

function s.matfilter(c,e,tp)
    return c:IsLocation(LOCATION_ONFIELD+LOCATION_GRAVE)
end