--シャドール・ドラゴン
local s,id=GetID()
function s.initial_effect(c)
	--destroy
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(id,0))
	e2:SetCategory(CATEGORY_DESTROY+CATEGORY_DRAW)
	e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e2:SetProperty(EFFECT_FLAG_CARD_TARGET+EFFECT_FLAG_DELAY)
	e2:SetCode(EVENT_TO_GRAVE)
	e2:SetCountLimit(1,id)
	e2:SetCondition(s.descon)
	e2:SetTarget(s.destg)
	e2:SetOperation(s.desop)
	c:RegisterEffect(e2)
end

function s.descon(e,tp,eg,ep,ev,re,r,rp)
	local c = e:GetHandler()
	return c:IsPreviousLocation(LOCATION_HAND) or c:IsPreviousLocation(LOCATION_FIELD)
end
function s.filter(c)
	return c:IsFaceup()
end
function s.destg(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	if chkc then return chkc:IsOnField() and s.filter(chkc) and chck:IsControler(tp) end
	if chk==0 then return Duel.IsExistingTarget(s.filter,tp,LOCATION_ONFIELD,0,1,nil) and Duel.GetFieldGroupCount(tp, LOCATION_HAND,0)>0 
		and Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0)>0 end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_DESTROY)
	local g=Duel.SelectTarget(tp,s.filter,tp,LOCATION_ONFIELD,0,1,1,nil)
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,g,1,0,0)
    Duel.SetOperationInfo(0,CATEGORY_DRAW,nil,0,tp,1)
end
function s.desop(e,tp,eg,ep,ev,re,r,rp)
	local tc=Duel.GetFirstTarget()
	if not tc:IsRelateToEffect(e) then return end
    local handCount = Duel.GetFieldGroupCount(tp, LOCATION_HAND,0)
    local deckCount = Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0)
	if Duel.Destroy(tc,REASON_EFFECT)<1 or handCount<1 or deckCount <1 then return end
    local g = Group.CreateGroup()
    if handCount==1 then g = Duel.GetMatchingGroup(Card.IsAbleToDeck, tp, LOCATION_HAND, 0, nil) 
    else 
        Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TODECK)
        g=Duel.SelectMatchingCard(tp,Card.IsAbleToDeck,tp,LOCATION_HAND,0,1,3,nil) 
    end
	if g==nil or #g<1 then return end
    Duel.SendtoDeck(g, nil, 1, REASON_EFFECT)
	Duel.BreakEffect()
    local drawCount = g:FilterCount(Card.IsLocation, nil, LOCATION_DECK)+1
    if drawCount<1 then return end
	Duel.Draw(tp,drawCount,REASON_EFFECT)
end
