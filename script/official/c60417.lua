--バトルマニア

local s,id=GetID()
function s.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetHintTiming(0,TIMING_BATTLE_START,TIMING_BATTLE_END)
	e1:SetCondition(s.condition)
	e1:SetTarget(s.target)
	e1:SetOperation(s.activate)
	c:RegisterEffect(e1)
end
function s.condition(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetTurnPlayer()~=tp and (Duel.GetCurrentPhase()>=PHASE_BATTLE_START and Duel.GetCurrentPhase()<=PHASE_BATTLE)
		and Duel.IsExistingMatchingCard(s.cfilter,tp,LOCATION_MZONE,0,1,nil)
        and Duel.GetFieldGroupCount(tp, LOCATION_MZONE, 0)<Duel.GetFieldGroupCount(tp, 0, LOCATION_MZONE)
end

function s.cfilter(c)
    return c:IsSetCard(0x101b) and c:IsFaceup()
end

function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
		and Duel.IsPlayerCanSpecialSummonMonster(tp,60399,0x101b,TYPES_TOKEN,1000,1000,2,RACE_MACHINE,ATTRIBUTE_WIND) end
	Duel.SetOperationInfo(0,CATEGORY_TOKEN,nil,nil,tp,0)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,nil,tp,0)
end
function s.activate(e,tp,eg,ep,ev,re,r,rp)
    local tpmzc = Duel.GetFieldGroupCount(tp, LOCATION_MZONE, 0)
    local oppmzc = Duel.GetFieldGroupCount(tp, 0, LOCATION_MZONE)
    if tpmzc>=oppmzc then return end
	local c = e:GetHandler()
    local lim = oppmzc-tpmzc
	for i=1,lim do
		if Duel.IsPlayerCanSpecialSummonMonster(tp,60399,0x101b,TYPES_TOKEN,1000,1000,1,RACE_MACHINE,ATTRIBUTE_WIND) and Duel.GetLocationCount(tp,LOCATION_MZONE)>0 then
			local token=Duel.CreateToken(tp,60399)
			Duel.SpecialSummonStep(token,0,tp,tp,false,false,POS_FACEUP)
            local fid=e:GetHandler():GetFieldID()
            token:RegisterFlagEffect(id,RESET_EVENT+RESETS_STANDARD,0,1,fid)
            local e1=Effect.CreateEffect(e:GetHandler())
            e1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
            e1:SetProperty(EFFECT_FLAG_IGNORE_IMMUNE)
            e1:SetCode(EVENT_PHASE+PHASE_END)
            e1:SetCountLimit(1)
            e1:SetLabel(fid)
            e1:SetLabelObject(token)
            e1:SetCondition(s.descon)
            e1:SetOperation(s.desop)
            --Duel.RegisterEffect(e1,tp)
		end
	end
	Duel.SpecialSummonComplete()
end

function s.descon(e,tp,eg,ep,ev,re,r,rp)
	local tc=e:GetLabelObject()
	if tc:GetFlagEffectLabel(id)==e:GetLabel() then
		return true
	else
		e:Reset()
		return false
	end
end
function s.desop(e,tp,eg,ep,ev,re,r,rp)
	local tc=e:GetLabelObject()
	Duel.Destroy(tc,REASON_EFFECT)
end
