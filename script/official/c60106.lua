--発条機甲ゼンマイスター
local s,id=GetID()
function s.initial_effect(c)
	--xyz summon
	Xyz.AddProcedure(c,nil,2,2)
	c:EnableReviveLimit()
	--atk
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e1:SetRange(LOCATION_MZONE)
	e1:SetCode(EFFECT_UPDATE_ATTACK)
	e1:SetValue(s.atkval)
	c:RegisterEffect(e1)
	--destroy
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e4:SetCode(EVENT_BATTLED)
    e4:SetCondition(s.condition)
	e4:SetOperation(s.operation)
    c:RegisterEffect(e4)
end
function s.atkval(e,c)
	return e:GetHandler():GetOverlayCount()*800
end

function s.condition(e,tp,eg,ep,ev,re,r,rp)
    return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_EFFECT)
end

function s.operation(e,tp,eg,ep,ev,re,r,rp)
	if not e:GetHandler():IsFaceup() then return end
    if not e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_EFFECT) then return end
	e:GetHandler():RemoveOverlayCard(tp,1,1,REASON_EFFECT)
end