local s,id=GetID()
function s.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetCode(EVENT_SPSUMMON_SUCCESS)
	e1:SetProperty(EFFECT_FLAG_DELAY)
	e1:SetCondition(s.condition)
	e1:SetTarget(s.target)
	e1:SetOperation(s.activate)
	c:RegisterEffect(e1)
end
s.listed_names={CARD_DARK_MAGICIAN_GIRL,CARD_DARK_MAGICIAN}
function s.cfilter(c,tp)
	return c:IsFaceup() and c:IsSummonPlayer(tp) and (c:IsCode(CARD_DARK_MAGICIAN_GIRL) )
end
function s.cfilter2(c,tp)
	return c:IsFaceup() and c:IsSummonPlayer(tp) and (c:IsCode(CARD_DARK_MAGICIAN))
end
function s.condition(e,tp,eg,ep,ev,re,r,rp)
	return eg:IsExists(s.cfilter,1,nil,tp) or eg:IsExists(s.cfilter2,1,nil,tp)
end
function s.filter(c,e,tp)
	return (c:IsCode(CARD_DARK_MAGICIAN_GIRL) ) and c:IsCanBeSpecialSummoned(e,0,tp,false,false)
end
function s.filter2(c,e,tp)
	return (c:IsCode(CARD_DARK_MAGICIAN)) and c:IsCanBeSpecialSummoned(e,0,tp,false,false)
end
function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
	local dmgchk = eg:IsExists(s.cfilter,1,nil,tp)
	local dmchk = eg:IsExists(s.cfilter2,1,nil,tp)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
		and ((dmgchk and Duel.IsExistingMatchingCard(s.filter2,tp,LOCATION_DECK,0,1,nil,e,tp)) or
		(dmchk and Duel.IsExistingMatchingCard(s.filter,tp,LOCATION_DECK,0,1,nil,e,tp))) end
	if dmchk and dmgchk then e:SetLabel(3)
	elseif dmgchk then e:SetLabel(1) elseif
		dmchk then e:SetLabel(2) end
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,0,tp,LOCATION_DECK)
end
function s.activate(e,tp,eg,ep,ev,re,r,rp)
	if Duel.GetLocationCount(tp,LOCATION_MZONE)<=0 then return end
	local op
	if e:GetLabel()==3 then
		op = Duel.SelectOption(tp,aux.Stringid(id,0),aux.Stringid(id,1))
	elseif e:GetLabel()==2 then op=0
	elseif e:GetLabel()==1 then op=1 end
	local using_filter = s.filter
	if op==1 then using_filter=s.filter2 end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
	local g=Duel.SelectMatchingCard(tp,using_filter,tp,LOCATION_DECK,0,1,1,nil,e,tp)
	if #g>0 then
		Duel.SpecialSummon(g,0,tp,tp,false,false,POS_FACEUP)
	end
end