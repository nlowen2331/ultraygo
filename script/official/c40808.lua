local s,id=GetID()

function s.initial_effect(c)
	--special summon
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_SPSUMMON_PROC)
	e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e1:SetRange(LOCATION_HAND)
    e1:SetCondition(s.spcon)
    e1:SetCountLimit(1,id)
	c:RegisterEffect(e1)
	--get GEAR
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_QUICK_O)
	e2:SetCode(EVENT_FREE_CHAIN)
	e2:SetCountLimit(1)
	e2:SetRange(LOCATION_MZONE)
	e2:SetTarget(s.addtg)
	e2:SetOperation(s.addop)
	c:RegisterEffect(e2)
	--discard to add
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(id,0))
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e3:SetProperty(EFFECT_FLAG_DELAY)
	e3:SetCode(EVENT_SUMMON_SUCCESS)
	e3:SetTarget(s.thtg)
	e3:SetOperation(s.thop)
	c:RegisterEffect(e3)
	local e4=e3:Clone()
	e4:SetCode(EVENT_SPSUMMON_SUCCESS)
	c:RegisterEffect(e4)
end

function s.spcon(e,c)
	if c==nil then return true end
	local tp=c:GetControler()
	return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
end

function s.cfilter(c)
	return c:IsSetCard(0x10ee) and c:IsType(TYPE_EQUIP)
end
function s.cfilter3(c)
	return c:IsFaceup() and c:IsCode(40803)
end
function s.cfilter2(c,ec)
	return c:IsCode(40803) and ec:CheckEquipTarget(c) and c:IsFaceup()
end

function s.addtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.IsExistingMatchingCard(s.cfilter, tp, LOCATION_HAND, 0, 1, nil) and 
		Duel.GetLocationCount(tp,LOCATION_SZONE)>0 and 
		Duel.IsExistingMatchingCard(s.cfilter3, tp, LOCATION_MZONE, 0, 1, nil) end
end

function s.addop(e,tp,eg,ep,ev,re,r,rp,chk)
	local gearchk = Duel.IsExistingMatchingCard(s.spellfilter, tp, LOCATION_DECK, 0, 1, nil) 
			and Duel.IsExistingMatchingCard(s.cfilter3, tp, LOCATION_MZONE, 0, 1, e:GetHandler())
				and Duel.GetLocationCount(tp,LOCATION_SZONE)>0
	if not gearchk then return end
	local ecg=Duel.SelectMatchingCard(tp,s.cfilter,tp,LOCATION_HAND,0,1,1,nil)
	local ec=ecg:GetFirst()
	local g1=Duel.SelectMatchingCard(tp,s.cfilter2,tp,LOCATION_MZONE,0,1,1,nil,ec)
	if #g1>0 and ec:CheckUniqueOnField(tp) and not ec:IsForbidden() 
		and Duel.GetLocationCount(tp,LOCATION_SZONE)>0 then
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_FACEUP)
		Duel.Equip(tp,ec,g1:GetFirst())
	end
end

function s.deckfilter(c,e,tp)
	return c:IsSetCard(0x10ee) and c:IsAbleToHand()
end
function s.thtg(e,tp,eg,ep,ev,re,r,rp,chk)
	local dchk = Duel.IsExistingMatchingCard(s.deckfilter,tp,LOCATION_DECK,0,1,nil)
	if chk==0 then return (dchk) end
	Duel.SetOperationInfo(0,CATEGORY_TOHAND,nil,1,tp,LOCATION_DECK)
end

function s.thop(e,tp,eg,ep,ev,re,r,rp)
	local count = Duel.GetMatchingGroupCount(s.deckfilter, tp, LOCATION_DECK, 0, nil) 
	if count<1 then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
	local g=Duel.SelectMatchingCard(tp,s.deckfilter,tp,LOCATION_DECK,0,1,1,nil)
	if g and #g>0 then
		Duel.SendtoHand(g,nil,REASON_EFFECT)
		Duel.ConfirmCards(1-tp,g)
	end
end