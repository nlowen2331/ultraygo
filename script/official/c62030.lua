--月光融合
--Lunalight Fusion
local s,id=GetID()
function s.initial_effect(c)
	local e1=Fusion.CreateSummonEff(c,aux.FilterBoolFunction(Card.IsSetCard,0xad),nil)
    e1:SetCost(s.cost)
    e1:SetHintTiming(0,TIMING_END_PHASE+TIMING_SUMMON+TIMING_FLIPSUMMON+TIMING_SPSUMMON)
	c:RegisterEffect(e1)
    --activate from hand
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_TRAP_ACT_IN_HAND)
	c:RegisterEffect(e2)
end

function s.cfilter(c)
	return c:IsSetCard(0xad) and c:IsType(TYPE_MONSTER) and c:IsType(TYPE_FUSION) and c:IsAbleToRemoveAsCost()
end

function s.cost(e,tp,eg,ep,ev,re,r,rp,chk)
    local c = e:GetHandler()
	local wantsToActivateFromHand = c:IsLocation(LOCATION_HAND)
    local didActivateFromHand = c:IsStatus(STATUS_ACT_FROM_HAND)
	if chk==0 and wantsToActivateFromHand then return Duel.IsExistingMatchingCard(s.cfilter,tp,LOCATION_GRAVE,0,1,nil) 
    elseif chk==0 and not wantsToActivateFromHand then return true end
    if not didActivateFromHand then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_REMOVE)
	local g=Duel.SelectMatchingCard(tp,s.cfilter,tp,LOCATION_GRAVE,0,1,1,nil)
	Duel.Remove(g,POS_FACEUP,REASON_COST)
end
