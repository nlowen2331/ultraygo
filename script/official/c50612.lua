--黒の魔導陣
--Dark Magical Circle
local s,id=GetID()
function s.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCountLimit(1,id+EFFECT_COUNT_CODE_OATH)
	e1:SetOperation(s.activate)
	c:RegisterEffect(e1)
    --atk/def up up up
    local e6=Effect.CreateEffect(c)
	e6:SetType(EFFECT_TYPE_FIELD)
	e6:SetRange(LOCATION_SZONE)
	e6:SetTargetRange(LOCATION_MZONE,0)
	e6:SetCode(EFFECT_UPDATE_ATTACK)
	e6:SetTarget(s.atkfilter)
	e6:SetValue(s.atkval)
    c:RegisterEffect(e6)
    local e7=e6:Clone()
    e7:SetCode(EFFECT_UPDATE_DEFENSE)
	c:RegisterEffect(e7)
	--draw
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(id,1))
	e2:SetCategory(CATEGORY_DRAW)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_TRIGGER_O)
	e2:SetCode(EVENT_SPSUMMON_SUCCESS)
	e2:SetRange(LOCATION_SZONE)
	e2:SetProperty(EFFECT_FLAG_DELAY+EFFECT_FLAG_PLAYER_TARGET)
	e2:SetCondition(s.drcon)
	e2:SetTarget(s.drtg)
	e2:SetOperation(s.drop)
	--c:RegisterEffect(e2)
end
function s.atkfilter(e,c)
	return true
end
function s.atkval(e,c)
	return Duel.GetMatchingGroupCount(s.valfilter, e:GetHandlerPlayer(), LOCATION_MZONE, 0, nil)*200
end
function s.valfilter(c)
	return c:IsSetCard(0x133) and c:IsFaceup()
end

function s.drcon(e,tp,eg,ep,ev,re,r,rp)
	return #eg==1 and eg:IsExists(s.cfilter,1,nil,tp)
end

function s.cfilter(c,tp)
    return c:IsFaceup() and c:IsSetCard(0x133) and c:IsControler(tp) and c:IsSummonPlayer(tp)
        and c:IsLevel(8)
end

function s.drtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
	Duel.SetTargetPlayer(tp)
	Duel.SetOperationInfo(0,CATEGORY_DRAW,nil,0,tp,1)
	Duel.SetOperationInfo(0,CATEGORY_TODECK,nil,1,tp,LOCATION_HAND)
end
function s.drop(e,tp,eg,ep,ev,re,r,rp)
	local p,d=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER,CHAININFO_TARGET_PARAM)
	Duel.Hint(HINT_SELECTMSG,p,HINTMSG_TODECK)
	local g=Duel.SelectMatchingCard(p,Card.IsAbleToDeck,p,LOCATION_HAND,0,1,2,nil)
	local todeck = Duel.SendtoDeck(g,nil,1,REASON_EFFECT)
	if todeck>0 then return end
	Duel.BreakEffect()
	Duel.Draw(p,todeck,REASON_EFFECT)
end

function s.exfilter(c)
	return c:IsSetCard(0x133) and c:IsType(TYPE_MONSTER) and c:IsAbleToHand()
		and c:IsLevel(8)
end

function s.activate(e,tp,eg,ep,ev,re,r,rp)
	local ct=3
	if Duel.GetFieldGroupCount(tp, LOCATION_DECK, 0)<ct then return end
	if not Duel.SelectYesNo(tp, aux.Stringid(id, 0)) then return end
	Duel.ConfirmDecktop(tp,ct)
	local g=Duel.GetDecktopGroup(tp,ct)
	if #g==0 then return end
	Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
	local sg = nil
	if g:IsExists(s.exfilter,1,nil) then
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
		sg=g:FilterSelect(tp,s.exfilter,1,1,nil)
	end
	Duel.DisableShuffleCheck()
	if sg and #sg>0 and sg:GetFirst():IsAbleToHand() then
		Duel.SendtoHand(sg,nil,REASON_EFFECT)
		Duel.ConfirmCards(1-tp,sg)
		Duel.ShuffleHand(tp)
		ct=ct-1
	end
	if ct>0 then Duel.SortDecktop(tp,tp,ct) end
end