local s,id=GetID()
function s.initial_effect(c)
	--Activate(summon)
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_DESTROY)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_SUMMON_SUCCESS)
	e1:SetTarget(s.target)
	e1:SetOperation(s.activate)
	c:RegisterEffect(e1)
	local e2=e1:Clone()
	e2:SetCode(EVENT_SPSUMMON_SUCCESS)
	c:RegisterEffect(e2)
end
function s.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	local tc=eg:GetFirst()
	local ag = Duel.GetMatchingGroup(aux.TRUE,tc:GetControler(),LOCATION_MZONE,0,tc)
	local cg = tc:GetColumnGroup(1,1)-tc:GetColumnGroup()
	local neg = ag-cg
	if chk==0 then return eg and tc and #eg==1 and tc:IsFaceup() 
		 and tc:IsOnField() and #neg>0 end
	Duel.SetOperationInfo(0,CATEGORY_DESTROY,neg,#neg,0,0)
end
function s.activate(e,tp,eg,ep,ev,re,r,rp)
	if #eg~=1 then return end
	local tc=eg:GetFirst()
	if not tc:IsOnField() then return end
	local ag = Duel.GetMatchingGroup(aux.TRUE,tc:GetControler(),LOCATION_MZONE,0,tc)
	local cg = tc:GetColumnGroup(1,1)-tc:GetColumnGroup()
	local neg = ag-cg
	if #neg>0 then
		Duel.Destroy(neg,REASON_EFFECT)
	end
end
