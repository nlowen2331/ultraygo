function c102.initial_effect(c)
	Synchro.AddProcedure(c,nil,1,1,Synchro.NonTuner(nil),1,99)
	c:EnableReviveLimit()
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD)
	e4:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e4:SetCode(EFFECT_CANNOT_ACTIVATE)
	e4:SetRange(LOCATION_MZONE)
	e4:SetTargetRange(0,1)
	e4:SetValue(c102.aclimit)
	e4:SetCondition(c102.actcon)
	c:RegisterEffect(e4)
end

function c102.aclimit(e,re,tp)
	return not re:GetHandler():IsImmuneToEffect(e)
end
function c102.actcon(e)
	return Duel.GetAttacker()==e:GetHandler()
end
