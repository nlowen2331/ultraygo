
--Nico
--Recapture

local s,id=GetID()
function s.initial_effect(c)
    --Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCountLimit(1)
	e1:SetCondition(s.missioncon)
	e1:SetTarget(function(e,tp,eg,ep,ev,re,r,rp,chk)
		if chk==0 then
			return true
		end
		if e:IsHasType(EFFECT_TYPE_ACTIVATE) then
			Duel.SetChainLimit(aux.FALSE)
		end
	end)
	e1:SetOperation(s.missionop)
	c:RegisterEffect(e1)
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e2:SetRange(LOCATION_SZONE)
	e2:SetCode(EVENT_PHASE+PHASE_BATTLE_START)
	e2:SetCountLimit(1)
	e2:SetOperation(s.capop)
    c:RegisterEffect(e2)
	local ge1=Effect.CreateEffect(c)
	ge1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	ge1:SetCode(EVENT_SPSUMMON_SUCCESS)
	ge1:SetProperty(EFFECT_FLAG_CANNOT_NEGATE)
	ge1:SetRange(LOCATION_SZONE)
	ge1:SetOperation(s.checkop)
	c:RegisterEffect(ge1)
	local ge2 = ge1:Clone()
	ge2:SetCode(EVENT_SUMMON_SUCCESS)
	c:RegisterEffect(ge2)
end

function s.checkop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if not c:IsFaceup() or not c:IsLocation(LOCATION_ONFIELD) then return end
	local rc=eg:GetFirst()
    for rc in aux.Next(eg) do
		if rc:IsSetCard(0xee) and (rc:IsRace(RACE_WARRIOR) or rc:IsRace(RACE_SPELLCASTER)) and rc:IsLevelAbove(3)
			and rc:IsControler(tp) and rc:IsPreviousControler(tp) then
			Duel.RegisterFlagEffect(tp,id,RESET_PHASE+PHASE_END,0,1) 
		end
	end
end

function s.capop(e,tp,eg,ep,ev,re,r,rp)
    Duel.SendtoGrave(e:GetHandler(),REASON_EFFECT)
    if Duel.GetFlagEffect(tp,id)<2 or not Duel.IsExistingMatchingCard(Card.IsCanChangePosition,tp,0,LOCATION_MZONE,1,nil,POS_FACEUP_DEFENSE)
        or Duel.GetLocationCount(tp,LOCATION_MZONE)<=0 then return end
	local c=e:GetHandler()
	local g2=Duel.GetMatchingGroup(Card.IsFaceup,tp,0,LOCATION_MZONE,nil)
	for tc in aux.Next(g2) do
		local e1=Effect.CreateEffect(c)
		e1:SetType(EFFECT_TYPE_SINGLE)
		e1:SetCode(EFFECT_UPDATE_ATTACK)
		e1:SetValue(-300)
		e1:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END)
		tc:RegisterEffect(e1)
		local e2 = e1:Clone()
		e2:SetCode(EFFECT_UPDATE_DEFENSE)
		tc:RegisterEffect(e2)
		Duel.NegateRelatedChain(tc,RESET_TURN_SET)
		local e3=Effect.CreateEffect(c)
		e3:SetType(EFFECT_TYPE_SINGLE)
		e3:SetCode(EFFECT_DISABLE)
		e3:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END)
		tc:RegisterEffect(e3)
		local e4=Effect.CreateEffect(c)
		e4:SetType(EFFECT_TYPE_SINGLE)
		e4:SetCode(EFFECT_DISABLE_EFFECT)
		e4:SetValue(RESET_TURN_SET)
		e4:SetReset(RESET_EVENT+RESETS_STANDARD+RESET_PHASE+PHASE_END)
        tc:RegisterEffect(e4)
	end
end

function s.missioncon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetFlagEffect(tp, 9652932)<1 and Duel.IsAbleToEnterBP()
end
function s.missionop(e,tp,eg,ep,ev,re,r,rp)
	Duel.RegisterFlagEffect(tp,9652932,RESET_PHASE+PHASE_END,0,1) 
end
