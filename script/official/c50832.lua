local s,id=GetID()

function s.initial_effect(c)
	--special summon
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetCode(EFFECT_SPSUMMON_PROC)
	e1:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e1:SetRange(LOCATION_HAND)
    e1:SetCondition(s.spcon)
	c:RegisterEffect(e1)
    --Prevent destruction by opponent's effect
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_FIELD)
	e3:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e3:SetRange(LOCATION_MZONE)
	e3:SetTargetRange(LOCATION_MZONE,0)
	e3:SetTarget(s.tgtg)
	e3:SetValue(s.efilter)
	c:RegisterEffect(e3)
	
end

function s.spcon(e,c)
	if c==nil then return true end
	local tp=c:GetControler()
	return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
end

function s.tgtg(e,c)
    local cg = e:GetHandler():GetColumnGroup(1,1)
	return c~=e:GetHandler() and cg:IsContains(c) and c:IsFaceup() and c:IsSetCard(0x2a)
end

function s.efilter(e,te)
	if te:IsActiveType(TYPE_SPELL+TYPE_TRAP) and te:GetHandlerPlayer()~=e:GetHandlerPlayer() then return true end
end
