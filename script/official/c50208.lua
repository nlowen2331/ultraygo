local s, id = GetID()
function s.initial_effect(c)
    c:EnableReviveLimit()
    Xyz.AddProcedure(c, s.matfilter, 4, 2)
    --act limit
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_FIELD)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetCode(EFFECT_CANNOT_ACTIVATE)
	e1:SetRange(LOCATION_MZONE)
	e1:SetTargetRange(0,1)
	e1:SetValue(s.aclimit)
	e1:SetCondition(s.condition)
    c:RegisterEffect(e1)
    --atk
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e2:SetCode(EFFECT_UPDATE_ATTACK)
	e2:SetRange(LOCATION_MZONE)
	e2:SetValue(s.atkval)
    c:RegisterEffect(e2)
    --maintenance
	local e6=Effect.CreateEffect(c)
	e6:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e6:SetCode(EVENT_PHASE+PHASE_END)
	e6:SetRange(LOCATION_MZONE)
	e6:SetCountLimit(1)
	e6:SetCondition(s.matcon)
	e6:SetOperation(s.matop)
	c:RegisterEffect(e6)
end

function s.matfilter(c, xyz, sumtype, tp)
    return c:IsRace(RACE_SPELLCASTER)
end

function s.atkval(e,c)
	return Duel.GetFieldGroupCount(c:GetControler(),0,LOCATION_ONFIELD)*100
end

function s.condition(e)
	return Duel.GetFieldGroupCount(e:GetHandlerPlayer(),0,LOCATION_ONFIELD)>Duel.GetFieldGroupCount(e:GetHandlerPlayer(),LOCATION_ONFIELD,0)
end
function s.aclimit(e,re,tp)
	return re:IsHasType(EFFECT_TYPE_ACTIVATE) and (re:IsActiveType(TYPE_SPELL) or re:IsActiveType(TYPE_TRAP))
end

function s.matcon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetTurnPlayer()==tp
end

function s.matop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
    if c:CheckRemoveOverlayCard(tp, 1, REASON_EFFECT) and Duel.SelectYesNo(tp, aux.Stringid(id, 0)) then
        c:RemoveOverlayCard(tp, 1, 1, REASON_EFFECT)
    else
        Duel.SendtoGrave(c, REASON_EFFECT)
    end
end

