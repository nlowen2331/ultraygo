local s, id = GetID()
function s.initial_effect(c)
    c:EnableReviveLimit()
    Xyz.AddProcedure(c, s.matfilter, 4, 2)
    --extroy
    local e2 = Effect.CreateEffect(c)
    e2:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_TRIGGER_O)
    e2:SetCode(EVENT_SPSUMMON_SUCCESS)
    e2:SetProperty(EFFECT_FLAG_DELAY + EFFECT_FLAG_CARD_TARGET)
    e2:SetCondition(s.excon)
    e2:SetCost(s.excost)
    e2:SetTarget(s.extg)
    e2:SetOperation(s.exop)
    c:RegisterEffect(e2, false, REGISTER_FLAG_DETACH_XMAT)
    --add counter
    local e0 = Effect.CreateEffect(c)
    e0:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
    e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
    e0:SetCode(EVENT_CHAINING)
    e0:SetRange(LOCATION_MZONE)
    e0:SetOperation(aux.chainreg)
    c:RegisterEffect(e0)
    local e1 = Effect.CreateEffect(c)
    e1:SetType(EFFECT_TYPE_CONTINUOUS + EFFECT_TYPE_FIELD)
    e1:SetCode(EVENT_CHAIN_SOLVING)
    e1:SetRange(LOCATION_MZONE)
    e1:SetOperation(s.acop)
    c:RegisterEffect(e1)
end

function s.matfilter(c, xyz, sumtype, tp)
    return c:IsRace(RACE_SPELLCASTER)
end

function s.excon(e, tp, eg, ep, ev, re, r, rp)
    return e:GetHandler():IsSummonType(SUMMON_TYPE_XYZ)
end
function s.excost(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return e:GetHandler():CheckRemoveOverlayCard(tp, 2, REASON_COST)
    end
    e:GetHandler():RemoveOverlayCard(tp, 2, 2, REASON_COST)
end

function s.thfilter(c)
    return c:IsType(TYPE_SPELL) and c:IsAbleToHand()
end
function s.extg(e, tp, eg, ep, ev, re, r, rp, chk)
    if chk == 0 then
        return Duel.IsPlayerCanDiscardDeck(tp, 5)
    end
end
function s.exop(e, tp, eg, ep, ev, re, r, rp)
    if not Duel.IsPlayerCanDiscardDeck(tp, 5) then
        return
    end
    Duel.ConfirmDecktop(tp, 5)
    local g = Duel.GetDecktopGroup(tp, 5)
    if g:GetCount() > 0 then
        Duel.DisableShuffleCheck()
        if g:IsExists(s.thfilter, 1, nil) then
            Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
            local sg = g:FilterSelect(tp, s.thfilter, 1, 1, nil)
            Duel.SendtoHand(sg, nil, REASON_EFFECT)
            Duel.ConfirmCards(1 - tp, sg)
            Duel.ShuffleHand(tp)
            g:Sub(sg)
        end
        Duel.SendtoDeck(g, nil, 2, REASON_EFFECT + REASON_REVEAL)
    end
    Duel.ShuffleDeck(tp)
end

function s.acop(e, tp, eg, ep, ev, re, r, rp)
    if re:IsActiveType(TYPE_SPELL) and e:GetHandler():GetFlagEffect(1) > 0 and re:GetHandlerPlayer() == tp then
        local c = e:GetHandler()
        local e1 = Effect.CreateEffect(c)
        e1:SetType(EFFECT_TYPE_SINGLE)
        e1:SetCode(EFFECT_UPDATE_ATTACK)
        e1:SetValue(400)
        e1:SetReset(RESET_EVENT + RESETS_STANDARD_DISABLE + RESET_PHASE + PHASE_END)
        c:RegisterEffect(e1)
    end
end
