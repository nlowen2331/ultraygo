local s,id=GetID()

function s.initial_effect(c)
	--special summon
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD)
	e2:SetCode(EFFECT_SPSUMMON_PROC)
	e2:SetProperty(EFFECT_FLAG_UNCOPYABLE)
	e2:SetRange(LOCATION_HAND)
    e2:SetCondition(s.spcon)
	c:RegisterEffect(e2)
    --sp summon token
	local e0=Effect.CreateEffect(c)
	e0:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_FIELD)
	e0:SetProperty(EFFECT_FLAG_CANNOT_DISABLE)
	e0:SetCode(EVENT_CHAINING)
	e0:SetRange(LOCATION_MZONE)
	e0:SetOperation(aux.chainreg)
	c:RegisterEffect(e0)
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_FIELD)
	e1:SetCode(EVENT_CHAIN_SOLVING)
	e1:SetProperty(EFFECT_FLAG_DELAY)
	e1:SetRange(LOCATION_MZONE)
	e1:SetOperation(s.acop)
	c:RegisterEffect(e1)
end

function s.spcon(e,c)
	if c==nil then return true end
	local tp=c:GetControler()
	return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
end

function s.acop(e,tp,eg,ep,ev,re,r,rp)
    local rc = re:GetHandler()
    local c = e:GetHandler()
    local loc=Duel.GetChainInfo(ev,CHAININFO_TRIGGERING_LOCATION)
	if rp~=tp  and loc==LOCATION_MZONE and c:GetFlagEffect(1)>0 
        and c:GetFlagEffect(2010)<2 then
        for i=1,1 do
            if Duel.IsPlayerCanSpecialSummonMonster(tp,60399,0x101b,TYPES_TOKEN,1000,1000,1,RACE_MACHINE,ATTRIBUTE_WIND) and Duel.GetLocationCount(tp,LOCATION_MZONE)>0
                and Duel.SelectYesNo(tp, aux.Stringid(id, 1)) then
				Duel.Hint(HINT_CARD,0,id)
                local token=Duel.CreateToken(tp,60399)
                Duel.SpecialSummonStep(token,0,tp,tp,false,false,POS_FACEUP)
				Duel.SpecialSummonComplete() 
                c:RegisterFlagEffect(2010, RESETS_STANDARD+RESET_PHASE+PHASE_END, 0, 1)
            end
        end   
	end
end


