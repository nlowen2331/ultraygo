--黒魔術のヴェール
local s,id=GetID()
function s.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON+CATEGORY_TOHAND)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
    e1:SetCode(EVENT_FREE_CHAIN)
    e1:SetCountLimit(1,id+EFFECT_COUNT_CODE_OATH)
	e1:SetTarget(s.target)
	e1:SetOperation(s.activate)
    c:RegisterEffect(e1)
    --draw
    local e2=Effect.CreateEffect(c)
    e2:SetCategory(CATEGORY_DRAW)
	e2:SetDescription(aux.Stringid(id,0))
	e2:SetType(EFFECT_TYPE_IGNITION)
	e2:SetRange(LOCATION_GRAVE)
	e2:SetCountLimit(1,id)
	e2:SetCost(s.plcost)
	e2:SetTarget(s.drtg)
	e2:SetOperation(s.drop)
	--c:RegisterEffect(e2)
end

function s.spfilter(c,e,tp)
	return c:IsSetCard(0x133) and c:IsLevel(3) and c:IsCanBeSpecialSummoned(e,0,tp,false,false)
end
function s.filter(c)
    return c:IsSetCard(0x133) and c:IsType(TYPE_MONSTER) and c:IsAbleToDeck()
        and Duel.IsExistingMatchingCard(s.filter2, c:GetControler(), LOCATION_DECK, 0, 1, nil,c:GetLevel())
end
function s.filter2(c,lv)
    return c:IsSetCard(0x133) and c:IsType(TYPE_MONSTER) and c:IsAbleToHand()
        and c:GetLevel()~=lv
end

function s.target(e,tp,eg,ep,ev,re,r,rp,chk)
    local g = Duel.GetMatchingGroup(s.spfilter, tp, LOCATION_HAND, 0, g1,e,tp)
    if chk==0 then return (Duel.GetLocationCount(tp,LOCATION_MZONE)>0 and #g>0) or
        (Duel.IsExistingMatchingCard(s.filter,tp,LOCATION_HAND+LOCATION_MZONE,0,1,nil) 
        and Duel.IsPlayerCanDraw(tp,1) ) end
end
function s.activate(e,tp,eg,ep,ev,re,r,rp)
    local g = Duel.GetMatchingGroup(s.spfilter, tp, LOCATION_HAND, 0, g1,e,tp)
    local cansp= (Duel.GetLocationCount(tp,LOCATION_MZONE)>0 and #g>0) 
    local canshuffle = (Duel.IsExistingMatchingCard(s.filter,tp,LOCATION_HAND+LOCATION_MZONE,0,1,nil) 
                        and Duel.IsPlayerCanDraw(tp,1) )
    if not cansp and not canshuffle then return end
    Duel.Hint(HINT_SELECTMSG,tp,0)
    local op=nil
    if cansp and canshuffle then op=Duel.SelectOption(tp,aux.Stringid(id,0),aux.Stringid(id,1))
    elseif cansp then op=0
    elseif canshuffle then op=1
    else return end
    if op==0 then
        Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
        local g=Duel.SelectMatchingCard(tp,s.spfilter,tp,LOCATION_HAND,0,1,1,nil,e,tp)
        if #g>0 then
            if Duel.SpecialSummon(g,0,tp,tp,false,false,POS_FACEUP)>0 then
            end
        end
    elseif op==1 then
        Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TODECK)
        local g=Duel.SelectMatchingCard(tp,s.filter,tp,LOCATION_MZONE+LOCATION_HAND,0,1,1,nil)
        if #g<1 then return end
        local tc = g:GetFirst()
        local lv = tc:GetLevel()
        Duel.ConfirmCards(1-tp, tc)
        if Duel.SendtoDeck(tc,nil,2,REASON_EFFECT)<1 then return end
        Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_ATOHAND)
        local g=Duel.SelectMatchingCard(tp,s.filter2,tp,LOCATION_DECK,0,1,1,nil,lv)
        if #g>0 then
            if Duel.SendtoHand(g,nil,REASON_EFFECT)>0 then 
                Duel.ConfirmCards(1-tp,g)
                Duel.ShuffleDeck(tp)
                Duel.Draw(tp, 1, REASON_EFFECT)
            end
        end
    end
end

function s.plcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():IsAbleToDeckAsCost() end
	Duel.SendtoDeck(e:GetHandler(),nil,1,REASON_COST)
end

function s.drtg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return true end
    Duel.SetTargetPlayer(tp)
    Duel.SetTargetParam(1)
	Duel.SetOperationInfo(0,CATEGORY_DRAW,nil,0,tp,1)
	Duel.SetOperationInfo(0,CATEGORY_TODECK,nil,1,tp,LOCATION_HAND)
end
function s.drop(e,tp,eg,ep,ev,re,r,rp)
	local p,d=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER,CHAININFO_TARGET_PARAM)
	Duel.Hint(HINT_SELECTMSG,p,HINTMSG_TODECK)
	local g=Duel.SelectMatchingCard(p,Card.IsAbleToDeck,p,LOCATION_HAND,0,1,1,nil)
	local todeck = Duel.SendtoDeck(g,nil,1,REASON_EFFECT)
	if todeck<1 then return end
	Duel.BreakEffect()
	Duel.Draw(p,d,REASON_EFFECT)
end