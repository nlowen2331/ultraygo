--デストーイ・シザー・ウルフ
--Frightfur Wolf
local s,id=GetID()
function s.initial_effect(c)
	--fusion material
	c:EnableReviveLimit()
    Fusion.AddProcMixRep(c,true,true,s.mfilter2,1,99,s.mfilter1)
	--multi
	local e3=Effect.CreateEffect(c)
	e3:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_CONTINUOUS)
	e3:SetCode(EVENT_SPSUMMON_SUCCESS)
	e3:SetOperation(s.atkop)
	c:RegisterEffect(e3)
end
function s.mfilter2(c,fc,sumtype,tp)
	return c:IsSetCard(0xa9,fc,sumtype,tp)
end
function s.mfilter1(c,fc,sumtype,tp)
	return c:IsSetCard(0xc3,fc,sumtype,tp) 
end
function s.countfilter(c)
	return c:IsSetCard(0xa9)
end
function s.atkop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	local e1=Effect.CreateEffect(c)
    local matGroup = c:GetMaterial()
    local attackNum = 0
    if matGroup~=nil then attackNum = matGroup:FilterCount(s.countfilter,nil) end
	if attackNum < 1 then return end
    local descNum = attackNum
    if descNum>4 then descNum=4 end
	e1:SetType(EFFECT_TYPE_SINGLE)
	e1:SetCode(EFFECT_EXTRA_ATTACK)
	e1:SetValue(attackNum)
	e1:SetReset(RESET_EVENT+RESETS_STANDARD_DISABLE)
	c:RegisterEffect(e1)
end
