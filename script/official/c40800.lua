--Nico
--Assault

local s,id=GetID()
function s.initial_effect(c)
    --Activate
	local e1=Effect.CreateEffect(c)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetCondition(s.missioncon)
	e1:SetTarget(function(e,tp,eg,ep,ev,re,r,rp,chk)
		if chk==0 then
			return true
		end
		if e:IsHasType(EFFECT_TYPE_ACTIVATE) then
			Duel.SetChainLimit(aux.FALSE)
		end
	end)
	e1:SetOperation(s.missionop)
	c:RegisterEffect(e1)
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	e2:SetRange(LOCATION_SZONE)
	e2:SetCode(EVENT_PHASE+PHASE_END)
	e2:SetCountLimit(1)
	e2:SetOperation(s.spop)
    c:RegisterEffect(e2)
	local ge1=Effect.CreateEffect(c)
	ge1:SetType(EFFECT_TYPE_FIELD+EFFECT_TYPE_CONTINUOUS)
	ge1:SetCode(EVENT_BATTLE_DESTROYING)
	ge1:SetProperty(EFFECT_FLAG_CANNOT_NEGATE)
	ge1:SetRange(LOCATION_SZONE)
	ge1:SetOperation(s.checkop)
	c:RegisterEffect(ge1)
end

function s.checkop(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	if not c:IsFaceup() or not c:IsLocation(LOCATION_ONFIELD) then return end
	local rc=eg:GetFirst()
    for rc in aux.Next(eg) do
		if rc:IsStatus(STATUS_OPPO_BATTLE) then
			if rc:IsRelateToBattle() then
				if rc:IsControler(tp) and rc:IsCode(40803) then Duel.RegisterFlagEffect(tp,id,RESET_PHASE+PHASE_END,0,1) end
			else
				if rc:IsPreviousControler(tp) and rc:IsCode(40803) then Duel.RegisterFlagEffect(tp,id,RESET_PHASE+PHASE_END,0,1) end
			end
		end
	end
end

function s.filter2(c,e,tp)
	return c:IsCode(40806) and c:IsCanBeSpecialSummoned(e,0,tp,false,false)
end

function s.spop(e,tp,eg,ep,ev,re,r,rp)
    Duel.SendtoGrave(e:GetHandler(),REASON_EFFECT)
    if Duel.GetFlagEffect(tp,id)<2 or not Duel.IsExistingMatchingCard(s.filter2,tp,LOCATION_HAND+LOCATION_DECK,0,1,nil,e,tp)
        or Duel.GetLocationCount(tp,LOCATION_MZONE)<=0 then return end
    Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_SPSUMMON)
    local g=Duel.SelectMatchingCard(tp,s.filter2,tp,LOCATION_HAND+LOCATION_DECK,0,1,1,nil,e,tp)
    if #g>0 then
        Duel.SpecialSummon(g,0,tp,tp,false,false,POS_FACEUP)
    end
end

function s.missioncon(e,tp,eg,ep,ev,re,r,rp)
	return Duel.GetFlagEffect(tp, 9652932)<1 and Duel.IsAbleToEnterBP()
end
function s.missionop(e,tp,eg,ep,ev,re,r,rp)
	Duel.RegisterFlagEffect(tp,9652932,RESET_PHASE+PHASE_END,0,1) 
end
