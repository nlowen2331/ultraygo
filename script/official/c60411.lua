--幻獣機ヤクルスラーン
local s,id=GetID()
function s.initial_effect(c)
	--synchro summon
	Synchro.AddProcedure(c,aux.FilterBoolFunctionEx(Card.IsSetCard,0x101b),1,1,Synchro.NonTunerEx(aux.TRUE,nil),1,99)
	c:EnableReviveLimit()
    --bd
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_TOKEN+CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e1:SetProperty(EFFECT_FLAG_DELAY)
	e1:SetCode(EVENT_BATTLE_DAMAGE)
	e1:SetCondition(s.tokcon)
	e1:SetTarget(s.toktg)
	e1:SetOperation(s.tokop)
	c:RegisterEffect(e1)
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_SINGLE)
	e2:SetCode(EFFECT_PIERCE)
	e2:SetValue(1)
	c:RegisterEffect(e2)
	--atk
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD)
	e4:SetCode(EFFECT_UPDATE_ATTACK)
	e4:SetRange(LOCATION_MZONE)
	e4:SetTargetRange(LOCATION_MZONE,0)
	e4:SetTarget(s.eftg)
	e4:SetValue(600)
	c:RegisterEffect(e4)
    local e5=e4:Clone()
    e5:SetCode(EFFECT_UPDATE_DEFENSE)
    c:RegisterEffect(e5)
	--no effect dest
	local e6=Effect.CreateEffect(c)
	e6:SetType(EFFECT_TYPE_SINGLE)
	e6:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e6:SetRange(LOCATION_MZONE)
	e6:SetCode(EFFECT_INDESTRUCTABLE_EFFECT)
	e6:SetCondition(s.indcon)
	e6:SetValue(s.indval)
	c:RegisterEffect(e6)
	local e7 = e6:Clone()
	e7:SetCode(EFFECT_INDESTRUCTABLE_BATTLE)
	e7:SetValue(1)
	c:RegisterEffect(e7)
	local e8=Effect.CreateEffect(c)
	e8:SetType(EFFECT_TYPE_FIELD)
	e8:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e8:SetCode(EFFECT_CANNOT_ACTIVATE)
	e8:SetRange(LOCATION_MZONE)
	e8:SetTargetRange(0,1)
	e8:SetValue(s.aclimit)
	e8:SetCondition(s.actcon)
	c:RegisterEffect(e8)
end

function s.aclimit(e,re,tp)
	return not re:GetHandler():IsImmuneToEffect(e)
end
function s.actcon(e)
	return Duel.GetAttacker()==e:GetHandler()
end


function s.indval(e,re,tp)
	return tp~=e:GetHandlerPlayer()
end

function s.tokfilter(c)
	return c:IsSetCard(0x101b) and c:IsType(TYPE_TOKEN)
end

function s.indcon(e)
	return Duel.IsExistingMatchingCard(s.tokfilter,e:GetHandlerPlayer(),LOCATION_MZONE,0,1,nil)
end

function s.tokcon(e,tp,eg,ep,ev,re,r,rp)
	return ep~=tp
end

function s.toktg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
		and Duel.IsPlayerCanSpecialSummonMonster(tp,60399,0x101b,TYPES_TOKEN,1000,1000,2,RACE_MACHINE,ATTRIBUTE_WIND) end
	Duel.SetOperationInfo(0,CATEGORY_TOKEN,nil,nil,tp,0)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,nil,tp,0)
end
function s.tokop(e,tp,eg,ep,ev,re,r,rp)
	local c = e:GetHandler()
	for i=1,2 do
		if Duel.IsPlayerCanSpecialSummonMonster(tp,60399,0x101b,TYPES_TOKEN,1000,1000,1,RACE_MACHINE,ATTRIBUTE_WIND) and Duel.GetLocationCount(tp,LOCATION_MZONE)>0
			and Duel.SelectYesNo(tp, aux.Stringid(id, 1)) then
			local token=Duel.CreateToken(tp,60399)
			Duel.SpecialSummonStep(token,0,tp,tp,false,false,POS_FACEUP)
		end
	end
	Duel.SpecialSummonComplete()
end

function s.eftg(e,c)
	return c:IsSetCard(0x101b) and c:IsType(TYPE_TOKEN)
end