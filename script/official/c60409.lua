--幻獣機ドラゴサック
--Mecha Phantom Beast Dracossack

local s,id=GetID()
function s.initial_effect(c)
	--Must be properly summoned before reviving
	c:EnableReviveLimit()
	--Xyz summon procedure
	Xyz.AddProcedure(c,nil,6,2,nil,nil,99)
	--Special summon 2 tokens to your field
	local e3=Effect.CreateEffect(c)
	e3:SetDescription(aux.Stringid(id,0))
	e3:SetCategory(CATEGORY_SPECIAL_SUMMON+CATEGORY_TOKEN)
	e3:SetType(EFFECT_TYPE_IGNITION)
	e3:SetRange(LOCATION_MZONE)
	e3:SetCountLimit(1)
	e3:SetCost(s.spcost)
	e3:SetTarget(s.sptg)
	e3:SetOperation(s.spop)
	c:RegisterEffect(e3,false,REGISTER_FLAG_DETACH_XMAT)
    --destroy after battled
	local e4=Effect.CreateEffect(c)
	e4:SetCategory(CATEGORY_DESTROY)
	e4:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e4:SetCode(EVENT_BATTLED)
	e4:SetCondition(s.descon)
	e4:SetOperation(s.desop)
    c:RegisterEffect(e4)
    --destroy replace
	local e2=Effect.CreateEffect(c)
	e2:SetType(EFFECT_TYPE_CONTINUOUS+EFFECT_TYPE_SINGLE)
	e2:SetProperty(EFFECT_FLAG_SINGLE_RANGE)
	e2:SetRange(LOCATION_MZONE)
	e2:SetCode(EFFECT_DESTROY_REPLACE)
	e2:SetTarget(s.desreptg)
	c:RegisterEffect(e2)
end

function s.spcost(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return e:GetHandler():CheckRemoveOverlayCard(tp,1,REASON_COST) end
	e:GetHandler():RemoveOverlayCard(tp,1,1,REASON_COST)
end

function s.sptg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetLocationCount(tp,LOCATION_MZONE)>0
		and Duel.IsPlayerCanSpecialSummonMonster(tp,60399,0x101b,TYPES_TOKEN,1000,1000,2,RACE_MACHINE,ATTRIBUTE_WIND) end
	Duel.SetOperationInfo(0,CATEGORY_TOKEN,nil,nil,tp,0)
	Duel.SetOperationInfo(0,CATEGORY_SPECIAL_SUMMON,nil,nil,tp,0)
end
function s.spop(e,tp,eg,ep,ev,re,r,rp)
	local c = e:GetHandler()
	for i=1,3 do
		if Duel.IsPlayerCanSpecialSummonMonster(tp,60399,0x101b,TYPES_TOKEN,1000,1000,1,RACE_MACHINE,ATTRIBUTE_WIND) and Duel.GetLocationCount(tp,LOCATION_MZONE)>0
			and Duel.SelectYesNo(tp, aux.Stringid(id, 1)) then
			local token=Duel.CreateToken(tp,60399)
			Duel.SpecialSummonStep(token,0,tp,tp,false,false,POS_FACEUP)
		end
	end
	Duel.SpecialSummonComplete()
end

function s.descon(e,tp,eg,ep,ev,re,r,rp)
	local att= Duel.GetAttacker()
    local bt = att:GetBattleTarget()
	return att and bt
end

function s.desop(e,tp,eg,ep,ev,re,r,rp)
	local att = Duel.GetAttacker()
    local bt = att:GetBattleTarget()
    local c = e:GetHandler()
    local a,b
    if att==c then
        a=att
        b=bt
    else
        b=att
        a=bt
    end
	if not b or not b:IsRelateToBattle() then return false end
    Duel.Destroy(b,REASON_EFFECT)
end
function s.desreptg(e,tp,eg,ep,ev,re,r,rp,chk)
	local c=e:GetHandler()
	if chk==0 then return not c:IsReason(REASON_REPLACE) and (c:IsReason(REASON_BATTLE) or rp~=tp)
		and Duel.IsExistingMatchingCard(s.tfilter, tp, LOCATION_MZONE, 0, 1, e:GetHandler(),e) end
	if Duel.SelectEffectYesNo(tp,e:GetHandler(),96) then
		Duel.Hint(HINT_SELECTMSG,tp,HINTMSG_TOGRAVE)
		local g=Duel.SelectMatchingCard(tp, s.tfilter, tp, LOCATION_MZONE, 0, 1, 1,e:GetHandler(),e)
		Duel.Release(g,REASON_COST)
		return true
	else return false end
end
function s.tfilter(c,e)
	return  not c:IsImmuneToEffect(e) and not c:IsStatus(STATUS_DESTROY_CONFIRMED)
		and c:IsSetCard(0x101b) and c~=e:GetHandler() and c:IsType(TYPE_TOKEN)
		and c:IsReleasable()
end