local s, id = GetID()
function s.initial_effect(c)
	--add on summon
	local e5 = Effect.CreateEffect(c)
	e5:SetDescription(aux.Stringid(id, 0))
	e5:SetCategory(CATEGORY_TOHAND)
	e5:SetType(EFFECT_TYPE_SINGLE + EFFECT_TYPE_TRIGGER_O)
	e5:SetCode(EVENT_SUMMON_SUCCESS)
	e5:SetProperty(EFFECT_FLAG_DELAY)
	e5:SetTarget(s.thtg)
	e5:SetOperation(s.thop)
	c:RegisterEffect(e5)
	local e6 = e5:Clone()
	e6:SetCode(EVENT_SPSUMMON_SUCCESS)
	c:RegisterEffect(e6)
end

function s.deckfilter(c, e, tp)
	return (c:IsSetCard(0xa9) or c:IsSetCard(0xc3)) and c:IsType(TYPE_MONSTER) and c:IsAbleToHand()
end
function s.thtg(e, tp, eg, ep, ev, re, r, rp, chk)
	local dchk = Duel.IsExistingMatchingCard(s.deckfilter, tp, LOCATION_DECK, 0, 1, nil)
	if chk == 0 then
		return (dchk)
	end
	Duel.SetOperationInfo(0, CATEGORY_TOHAND, nil, 1, tp, LOCATION_DECK)
end

function s.thop(e, tp, eg, ep, ev, re, r, rp)
	if Duel.IsExistingMatchingCard(s.deckfilter, tp, LOCATION_DECK, 0, 1, nil) then
		local g = nil
		Duel.Hint(HINT_SELECTMSG, tp, HINTMSG_ATOHAND)
		g = Duel.SelectMatchingCard(tp, s.deckfilter, tp, LOCATION_DECK, 0, 1, 1, nil)
		if g and g:GetCount() > 0 then
			Duel.SendtoHand(g, nil, REASON_EFFECT)
			Duel.ConfirmCards(1 - tp, g)
		end
	end
end