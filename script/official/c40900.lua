--アーティファクト－デスサイズ
--Artifact Scythe
local s,id=GetID()
function s.initial_effect(c)
	local e2=Effect.CreateEffect(c)
	e2:SetDescription(aux.Stringid(id,0))
	e2:SetCategory(CATEGORY_HANDES)
	e2:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_F)
	e2:SetCode(EVENT_TO_GRAVE)
	e2:SetCondition(s.discon)
	e2:SetTarget(s.distg)
	e2:SetOperation(s.disop)
	c:RegisterEffect(e2)
end

function s.discon(e,tp,eg,ep,ev,re,r,rp)
	local c=e:GetHandler()
	return c:IsPreviousLocation(LOCATION_ONFIELD) and c:IsPreviousPosition(POS_FACEDOWN)
		and c:IsReason(REASON_DESTROY+REASON_EFFECT) and rp~=tp
end

function s.distg(e,tp,eg,ep,ev,re,r,rp,chk)
	if chk==0 then return Duel.GetFieldGroupCount(tp,0,LOCATION_HAND)>0 end
	Duel.SetOperationInfo(0,CATEGORY_HANDES,0,0,1-tp,2)
end
function s.disop(e,tp,eg,ep,ev,re,r,rp)
    local count = 2
	local g=Duel.GetFieldGroup(tp,0,LOCATION_HAND,nil)
    if #g==0 then return end
    if #g==1 then count=1 end
	local sg=g:RandomSelect(1-tp,count)
	Duel.SendtoGrave(sg,REASON_DISCARD+REASON_EFFECT)
end
