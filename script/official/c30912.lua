--死者転生
local s,id=GetID()
function s.initial_effect(c)
	--Activate
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_TOHAND)
	e1:SetType(EFFECT_TYPE_ACTIVATE)
	e1:SetCode(EVENT_FREE_CHAIN)
	e1:SetProperty(EFFECT_FLAG_PLAYER_TARGET)
	e1:SetTarget(s.target)
	e1:SetOperation(s.activate)
	c:RegisterEffect(e1)
end
function s.tgfilter(c)
	return c:IsSetCard(0x1034) and c:IsFaceup()
end
function s.filter(c)
	return c:IsSetCard(0x1034) and c:IsType(TYPE_MONSTER) and c:IsAbleToHand()
end
function s.target(e,tp,eg,ep,ev,re,r,rp,chk,chkc)
	local g = Duel.GetMatchingGroup(s.tgfilter,tp,LOCATION_SZONE,0,nil)
	if chk==0 then return #g>0 and Duel.IsPlayerCanDraw(tp,#g) end
	Duel.SetTargetPlayer(tp)
	Duel.SetOperationInfo(0,CATEGORY_DRAW,nil,0,tp,#g)
	Duel.SetOperationInfo(0,CATEGORY_TODECK,nil,#g-1,tp,LOCATION_HAND)
end
function s.activate(e,tp,eg,ep,ev,re,r,rp)
	local p,ex=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER,CHAININFO_TARGET_PARAM)
	local g = Duel.GetMatchingGroup(s.tgfilter,p,LOCATION_SZONE,0,nil)
	if #g<1 then return end
	if #g==0 then return end
	if Duel.Draw(p,#g,REASON_EFFECT)==#g then
		if #g==1 then return end
		Duel.Hint(HINT_SELECTMSG,p,HINTMSG_TODECK)
		local g2=Duel.SelectMatchingCard(p,Card.IsAbleToDeck,p,LOCATION_HAND,0,#g-1,#g-1,nil)
		if #g2==0 then return end
		Duel.SendtoDeck(g2,nil,2,REASON_EFFECT)
		Duel.ShuffleDeck(p)
	end
end

