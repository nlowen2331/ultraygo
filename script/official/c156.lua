function c156.initial_effect(c)
	--synchro summon
	Synchro.AddProcedure(c,aux.FilterBoolFunctionEx(Card.IsSetCard,0x79),1,1,Synchro.NonTuner(nil),1,99)
	c:EnableReviveLimit()
	--spsummon
	local e1=Effect.CreateEffect(c)
	e1:SetCategory(CATEGORY_SPECIAL_SUMMON)
	e1:SetType(EFFECT_TYPE_SINGLE+EFFECT_TYPE_TRIGGER_O)
	e1:SetCode(EVENT_SPSUMMON_SUCCESS)
	e1:SetProperty(EFFECT_FLAG_DELAY)
	e1:SetCondition(c156.con)
	e1:SetTarget(c156.target)
	e1:SetOperation(c156.operation)
	c:RegisterEffect(e1)
end

function c156.con(e,tp,eg,ep,ev,re,r,rp)
	return e:GetHandler():IsSummonType(SUMMON_TYPE_SYNCHRO)
end

function c156.cfilter(c)
	return c:IsSetCard(0x7c) and c:IsType(TYPE_SPELL+TYPE_TRAP) and c:IsFaceup() and c:IsAbleToDeck()
end

function c156.target(e,tp,eg,ep,ev,re,r,rp,chk)
	local count=Duel.GetMatchingGroupCount(c156.cfilter,tp,LOCATION_ONFIELD,0,nil)
	if chk==0 then return count>0 and Duel.IsPlayerCanDraw(tp,count+1) end
	Duel.SetTargetPlayer(tp)
	Duel.SetTargetParam(count+1)
	Duel.SetOperationInfo(0,CATEGORY_DRAW,nil,0,tp,count+1)
end
function c156.operation(e,tp,eg,ep,ev,re,r,rp)
	local p,d=Duel.GetChainInfo(0,CHAININFO_TARGET_PLAYER,CHAININFO_TARGET_PARAM)
	local g=Duel.GetMatchingGroup(c156.cfilter,tp,LOCATION_ONFIELD,0,nil)
	local sendCount=Duel.SendtoDeck(g,nil,2,REASON_EFFECT)
	Duel.ShuffleDeck(tp)
	Duel.BreakEffect()
	if sendCount >0 then
		Duel.Draw(p,sendCount+1,REASON_EFFECT)
	end
end