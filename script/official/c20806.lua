
local s,id=GetID()
function s.initial_effect(c)
	--synchro summon
	Synchro.AddProcedure(c,nil,1,1,Synchro.NonTunerEx(Card.IsAttribute,ATTRIBUTE_DARK),1,99)
	c:EnableReviveLimit()
	local e4=Effect.CreateEffect(c)
	e4:SetType(EFFECT_TYPE_FIELD)
	e4:SetCode(EFFECT_UPDATE_ATTACK)
	e4:SetRange(LOCATION_MZONE)
	e4:SetTargetRange(0,LOCATION_MZONE)
	e4:SetTarget(s.tg)
	e4:SetValue(s.val)
	c:RegisterEffect(e4)
end

function s.val(e,c)
	return Duel.GetMatchingGroupCount(s.cfilter,e:GetHandler():GetControler(),LOCATION_MZONE,0,nil)*-200
end

function s.cfilter(c)
	return c:IsAttribute(ATTRIBUTE_DARK) and c:IsFaceup()
end

function s.tg(e,c)
	return c:IsFaceup() and c:IsType(TYPE_MONSTER)
end

