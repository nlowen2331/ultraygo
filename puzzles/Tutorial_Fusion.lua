--Fusion Tutorial
--AlphaKretin
Debug.SetAIName("TutorialBot")
Debug.ReloadFieldBegin(DUEL_ATTACK_FIRST_TURN+DUEL_SIMPLE_AI+DUEL_PSEUDO_SHUFFLE,3)

Debug.SetPlayerInfo(0,4000,0,0)
Debug.SetPlayerInfo(1,6900,0,0)

--AI's field.
Debug.AddCard(30331,1,1,LOCATION_MZONE,2,POS_FACEUP_ATTACK) --Sabretooth

--Player's Field
Debug.AddCard(134,0,0,LOCATION_MZONE,2,POS_FACEUP_ATTACK) --E HERO Avian
Debug.AddCard(147,0,0,LOCATION_SZONE,2,POS_FACEUP) --E HERO Avian

--Below makes it a one turn puzzle.
aux.BeginPuzzle()

--LOCATION_DECK
--LOCATION_SZONE
--LOCATION_GRAVE
--LOCATION_HAND
--LOCATION_MZONE
--LOCATION_EXTRA
--LOCATION_REMOVED
--POS_FACEDOWN
--POS_FACEDOWN_DEFENSE
--POS_FACEUP
--POS_FACEUP_DEFENSE
--POS_FACEUP_ATTACK
--Debug.PreEquip(e1,c1)
--Debug.PreSummon(c,type)
--Debug.AddCard()
--aux.BeginPuzzle()
